# Équipements

Spécification du modèle de données relatif aux équipements collectifs publics d'une collectivité

- nom : `equipements`
- page d'accueil : https://gitlab.com/opendatafrance/scdl/equipements
- URL du schéma : https://gitlab.com/opendatafrance/scdl/equipements/raw/v0.1.5/schema.json
- version : `0.1.5`
- date de création : 20/11/2018
- date de dernière modification : 19/01/2024
- concerne le pays : FR
- valeurs manquantes représentées par : `[""]`
- contributeurs :
  - OpenDataFrance (auteur) [scdl@opendatafrance.email](scdl@opendatafrance.email)
  - Romain Buchaut (contributeur)
  - Charles Nepote (contributeur) [charles.nepote@fing.org](charles.nepote@fing.org)
  - Pierre Dittgen, Jailbreak (contributeur)
  - Quentin Loridant, multi (contributeur) [quentin.loridant@multi.coop](uentin.loridant@multi.coop)
  - Amélie Rondot, multi (contributrice) [amelie.rondot@multi.coop](amelie.rondot@multi.coop)
- sources :
  - Définition d&#x27;équipement (groupe de travail EquipCo) ([lien](http://www.crige-paca.org/index.php?eID=tx_crigedocuments&hash=2eb4b236&fid=3117))
  - Modèle de données des équipements collectifs publics (CRIGE PACA) ([lien](http://www.crige-paca.org/index.php?eID=tx_crigedocuments&hash=2eb4b236&fid=3117))
  - Nomenclature des équipements collectifs publics (CRIGE PACA) ([lien](http://www.crige-paca.org/index.php?eID=tx_crigedocuments&hash=97632e80&fid=3118))

## Modèle de données

Ce modèle de données repose sur les 21 champs suivants correspondant aux colonnes du fichier tabulaire.

### `COLL_NOM`

- titre : Nom de la collectivité
- description : Nom officiel de la collectivité sur le territoire de laquelle sont situés les équipements collectifs publics répertoriés dans le jeu de données. Ce nom est limité à 140 caractères maximum.
- type : chaîne de caractères
- exemple : `Département du Val-de-Marne`
- valeur obligatoire

### `COLL_SIRET`

- titre : Code SIRET de la collectivité
- description : Identifiant du [Système d'Identification du Répertoire des Etablissements](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements) (SIRET) de la collectivité sur le territoire de laquelle sont situés les équipements collectifs publics répertoriés dans le jeu de données. Il est composé de 9 chiffres SIREN + 5 chiffres NIC d’un seul tenant.
- type : chaîne de caractères
- exemple : `22940028800010`
- valeur obligatoire
- motif : `^\d{14}$`

### `EQUIP_UID`

- titre : Identifiant unique de l'équipement
- description : Cet identifiant unique est constitué du [code INSEE de la commune](https://fr.wikipedia.org/wiki/Code_Insee) où est implanté l'équipement sur 5 caractères (incluant 'A' ou 'B' pour la Corse) suivi du code d'identification de l'équipement (EQUIP_CODE), séparés par un tiret du milieu. Il s'agit donc d'une chaîne de 18 caractères qui permet d'identifier chacun des équipements référencés de manière univoque.
- type : chaîne de caractères (uuid)
- exemple : `94059-01010401-001`
- valeur obligatoire
- taille maximale : 18

### `EQUIP_THEME`

- titre : Thème de classement de l'équipement
- description : Les entrées de la [nomenclature des équipements collectifs publics](https://docs.google.com/spreadsheets/d/157WPWMUDC6w58Aep1dgWzzunKEjzSd-QmyuEHa8RFqc) sont divisées en 10 grandes familles. Les intitulés de ces grandes familles sont utilisés pour classer les équipements par thème. Ce champ doit donc être renseigné à partir d'une des valeurs suivantes : 'Equipement administratif', 'Equipement de justice', 'Equipement sanitaire', 'Equipement social et d'animation', 'Equipement sportif et de loisirs', 'Equipement d'enseignement', 'Equipement cultuel', 'Equipement culturel', 'Equipement de mobilité', ou 'Autre équipement'.
- type : chaîne de caractères
- exemple : `Equipement administratif`
- valeur obligatoire
- valeurs autorisées : `["Equipement administratif","Equipement de justice","Equipement sanitaire","Equipement social et d'animation","Equipement sportif et de loisirs","Equipement d'enseignement","Equipement cultuel","Equipement culturel","Equipement de mobilité","Autre équipement"]`

### `EQUIP_CODE`

- titre : Code d'identification de l'équipement
- description : Le code d'identification de l'équipement est constitué du code sur 8 chiffres des niveaux 3 ou 4 (quand il existe) de la [nomenclature des équipements collectifs publics](https://docs.google.com/spreadsheets/d/157WPWMUDC6w58Aep1dgWzzunKEjzSd-QmyuEHa8RFqc), suivi d'un numéro d'ordre sur 3 chiffres (de '001' minimum à '999' maximum), séparés par un tiret du milieu. Il est utilisé pour construire l'identifiant unique de l'équipement (EQUIP_UID). En fonction du niveau et donc du code choisi dans la nomenclature, un des termes associés doit être reporté en tant que valeur pour définir le type d'équipement (EQUIP_TYPE).
- type : chaîne de caractères
- exemple : `01010401-001`
- valeur obligatoire
- taille maximale : 12

### `EQUIP_TYPE`

- titre : Type d'équipement
- description : Le type d'équipement correspond à un des termes associés au code choisi dans la [nomenclature des équipements collectifs publics](https://docs.google.com/spreadsheets/d/157WPWMUDC6w58Aep1dgWzzunKEjzSd-QmyuEHa8RFqc) pour identifier l'équipement dans EQUIP_CODE. Il s'agit donc de renseigner ce champ avec une valeur, jugée la plus pertinente pour désigner l'équipement, dans la limite de 140 caractères maximum en prenant soin d'échapper ou de supprimer les éventuelles virgules.
- type : chaîne de caractères
- exemple : `Mairie`
- valeur obligatoire

### `EQUIP_NOM`

- titre : Nom complet de l'équipement
- description : Ce champ permet de nommer l'équipement collectif public par son nom d'usage complet afin de préciser ou compléter, si nécessaire, le terme utilisé pour désigner le type, dans la limite de 256 caractères maximum.
- type : chaîne de caractères
- exemple : `Hôtel de ville du Plessis-Trévise`
- valeur obligatoire

### `ADR_NUMERO`

- titre : Numéro d’adresse complet
- description : Ce champ désigne le numéro d’adresse dans la voie suivi, si nécessaire, d'une information suffixée qui complète et précise le numéro d’adresse. Cette information suffixée peut être un indice de répétition ('bis', 'ter', 'qua', 'qui', etc... codés sur 3 caractères en minuscules) ou un complément comme le nom d'entrée d'immeuble ('a', 'b', 'c', 'a1', 'b2', 'lesmimosas', etc... codés en minuscules non accentuées, sans espace ni limite du nombre de caractères). Dans le cas des voies ou des lieux-dits sans adresse, la valeur '99999' est attendue. Dans le cas d'une adresse indiquant un intervalle entre deux numéros, ces derniers sont séparés par une barre oblique.
- type : chaîne de caractères
- exemple : `'36' pour un numéro sans suffixe ou '36 bis' pour un numéro avec un indice de répétition ou '36/38' pour un intervalle entre deux numéros`
- valeur obligatoire

### `ADR_NOMVOIE`

- titre : Nom complet de la voie
- description : Ce champ contient la concaténation du type et du nom de la voie ou le nom d'un lieu-dit, exprimés en majuscules et minuscules accentuées.
- type : chaîne de caractères
- exemple : `Avenue Ardouin`
- valeur obligatoire
- taille minimale : 3
- motif : `^[a-zA-Z0-9\-\'\s\d\u00C0-\u00FF]+$`

### `ADR_CODEPOSTAL`

- titre : Code postal
- description : Elément de l'adresse qui désigne le code postal de la commune où est implanté l'équipement collectif public.
- type : chaîne de caractères
- exemple : `94420`
- valeur obligatoire
- taille maximale : 5

### `ADR_COMMUNE`

- titre : Commune
- description : Elément de l'adresse qui désigne le nom de la commune où est implanté l'équipement collectif public.
- type : chaîne de caractères
- exemple : `Le Plessis-Trévise`
- valeur obligatoire
- motif : `^[A-Za-z\s\-\u00C0-\u00FF]+$`

### `ADR_CLE_INTEROP`

- titre : Clé d'interopérabilité de l'adresse
- description : Cette clé est identique à celle décrite dans le modèle [Base adresse locale](https://scdl.opendatafrance.net/docs/schemas/scdl-adresses.html). Elle combine le [code INSEE de la commune](https://fr.wikipedia.org/wiki/Code_Insee) sur 5 caractères (incluant 'A' ou 'B' pour la Corse) + le code de voie issu du [FANTOIR](https://fr.wikipedia.org/wiki/FANTOIR) de la DGFiP sur 4 caractères + le numéro d’adresse sur 5 caractères préfixé par des zéros + un suffixe s'il existe, qui peut être un indice de répétition ('bis', 'ter', 'qua', 'qui', etc... codés sur 3 caractères) et/ou un complément ('a', 'b', 'c', 'a1', 'b2', 'lesmimosas', etc... sans limitation du nombre de caractères). Chaque élément est séparé par un tiret du bas et les lettres sont en minuscules.
- type : chaîne de caractères
- exemple : `94059_0040_00036`
- valeur optionnelle
- taille minimale : 16
- motif : `^[A-Za-z0-9_]+$`

### `ERP_ID`

- titre : Type d'Etablissement Recevant du Public
- description : Si l'équipement collectif public est un ERP et que son code d'identification, le plus souvent géré par les SDIS ou les DDT référents, est connu, ce champ peut être renseigné.
- type : chaîne de caractères
- exemple : `94059-AvenueArdouin-36`
- valeur optionnelle

### `ERP_TYPE`

- titre : Type d'Etablissement Recevant du Public
- description : Les [Etablissements Recevant du Public](https://fr.wikipedia.org/wiki/%C3%89tablissement_recevant_du_public_en_droit_fran%C3%A7ais) (ERP) installés dans un bâtiment et les établissements spéciaux sont classés par type en fonction de leur activité ou de la nature de leur exploitation. Le type est symbolisé par une à trois lettre(s) en majuscule dans le respect de [l'article GN1 de l'Arrêté du 25 juin 1980](https://www.legifrance.gouv.fr/affichTexte.do;?cidTexte=LEGITEXT000020303557). Si l'équipement collectif public est un ERP, ce champ peut être renseigné à partir d'une des valeurs suivantes :  'J', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y' pour les ERP installés dans un bâtiment et 'PA', 'CTS', 'SG', 'PS', 'GA', 'OA', 'EF', 'REF' pour les établissements spéciaux. Dans le cas d'un ERP couvrant plusieurs types, les valeurs sont séparées par un point-virgule.
- type : chaîne de caractères
- exemple : `'W' pour un ERP de type unique ou 'W;L' pour un ERP couvrant plusieurs types`
- valeur optionnelle
- taille maximale : 10

### `ERP_CATEGORIE`

- titre : Catégorie d'Etablissement Recevant du Public
- description : Les [Etablissements Recevant du Public](https://fr.wikipedia.org/wiki/%C3%89tablissement_recevant_du_public_en_droit_fran%C3%A7ais) (ERP) sont classés par catégorie en fonction de leur capacité d'accueil. La catégorie est symbolisée par un chiffre de 1 à 5 dans le respect de [l'article R123-19 du Code de la construction et de l'habitation](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074096&idArticle=LEGIARTI000006896108). Si l'équipement collectif public est un ERP, ce champ peut être renseigné.
- type : nombre entier
- exemple : `5`
- valeur optionnelle
- valeur minimale : 1
- valeur maximale : 5

### `EQUIP_LAT`

- titre : Latitude
- description : Coordonnée de latitude exprimée en [WGS 84](https://fr.wikipedia.org/wiki/WGS_84) permettant de localiser l'équipement collectif public. Le signe de séparation entre les parties entière et décimale du nombre est le point.
- type : nombre réel
- exemple : `48.808989`
- valeur obligatoire

### `EQUIP_LONG`

- titre : Longitude
- description : Un ou plusieurs mot(s) clé(s) utilisé(s) pour décrire le jeu de données en minuscules non accentuées. S'il y en a plusieurs, le séparateur est le point-virgule.
- type : nombre réel
- exemple : `2.572875`
- valeur obligatoire

### `EQUIP_OUVERTURE`

- titre : Jours et horaires d'ouverture
- description : Ce champ permet de renseigner, si l'information est connue, les jours et horaires d'ouverture de l'équipement en respectant le [format utilisé pour la clé 'opening_hours'](https://wiki.openstreetmap.org/wiki/FR:Key:opening_hours) dans OpenStreetMap. Un outil comme [YoHours](http://projets.pavie.info/yohours/) facilite la transformation des jours et horaires d'ouverture dans ce format. Celui-ci pouvant contenir des virgules comme signes de séparation, il est nécessaire d'entourer les valeurs de la chaîne de caractères par des guillemets doubles.
- type : chaîne de caractères
- exemple : `Mo-Fr 08:30-12:00,13:30-17:30; Sa 08:30-12:00`
- valeur optionnelle

### `EQUIP_TEL`

- titre : Téléphone
- description : Ce champ permet de renseigner, si l'information est connue, le numéro de téléphone (du gestionnaire) de l'équipement exprimé en suivant le [code de rédaction interinstitutionnel européen](http://publications.europa.eu/code/fr/fr-390300.htm).
- type : chaîne de caractères
- exemple : `+33 140633900`
- valeur optionnelle
- taille maximale : 13

### `EQUIP_EMAIL`

- titre : Adresse email
- description : Ce champ permet de renseigner, si l'information est connue, l'adresse email (du gestionnaire) de l'équipement.
- type : chaîne de caractères (email)
- exemple : `contact@leplessistrevise.fr`
- valeur optionnelle

### `EQUIP_WEB`

- titre : Adresse du site web
- description : Ce champ permet de renseigner, si l'information est connue, l'url d'accès au site web (du gestionnaire) de l'équipement.
- type : chaîne de caractères (uri)
- exemple : `https://www.leplessistrevise.fr`
- valeur optionnelle
